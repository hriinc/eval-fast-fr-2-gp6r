/*
 * Exercise 4
 *
 * Implement a new static observable operator called newCombineLatest.
 * newCombineLatest should behave just like combineLatest but take an object of
 * observable properties and emit an object where the properties have the value emitted for the respective
 * observable as shown below.
 *
 * Please also include in a comment (or write out to console) the output of the first 10 emissions of the example below with the implemented
 * newCombineLatest.
 *
 * It is required that you use an online editor (jsbin, codepen, jsfiddle, stackblitz, etc) and send a link as your submission. This
 * should be a separate link apart from the other exercises. Submissions that do not use an online editor or are not separate
 * links will not be considered.
 *
 * IMPORTANT: all exercises are best effort. If you cannot arrive at the working solution requested, please just include an explanation
 * as to your approach, what worked, what didn't, and anything else you feel may be relevant.
 */

newCombineLatest({
  foo: of(45),
  bar: interval(2000),
  baz: timer(1000),
  faz: from(1, 2, 3, 4)
}).pipe(
  tap(res => {
    console.log('foo', res.foo, ', bar: ', res.bar);
  }),
  take(4)
).subscribe();
