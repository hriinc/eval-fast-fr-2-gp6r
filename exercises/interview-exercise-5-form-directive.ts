/*
 * Exercise 5
 *
 * Implement an Angular directive that will automatically disable a form when a submission occurs and will accomplish the following:
 *
 * - is intended to be used on reactive forms with FormGroups
 * - should take one input: a function that when called will return one of the following:
 *    - Observable of type false or unknown
 *    - false
 *    - unknown
 * - when the form is submitted the input function will be called if an only if the form is valid
 * - upon submission of a valid form
 *    - the provided observable from the input function should be subscribed to
 *    - the form should be disabled for at least 2 seconds from form submission until either the observable emits successfully or an error occurs
 * - the directive should not stop working after one form submission regardless of whether or not the observable from the input erred.
 *
 * Anything else you see fit to demonstrate the working directive can be provided.
 *
 * It is required that you use an online editor (jsbin, codepen, jsfiddle, stackblitz, etc) and send a link as your submission. This
 * should be a separate link apart from the other exercises. Submissions that do not use an online editor or are not separate
 * links will not be considered.
 *
 * IMPORTANT: all exercises are best effort. If you cannot arrive at the working solution requested, please just include an explanation
 * as to your approach, what worked, what didn't, and anything else you feel may be relevant.
 */

