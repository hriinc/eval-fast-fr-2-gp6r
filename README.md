# Instructions
The exercises are in the `exercises` directory. For each exercise the instructions will be in a comment and the beginning of the file or in a respective README file. Please make sure to follow the directions carefully, especially the submission requirements. Submissions that do not follow the directions will not be considered. Thanks for taking the time to undergo the assessments and good luck.

**PLEASE DO NOT SUBMIT PULL REQUESTS TO THIS REPO**
